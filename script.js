const buttons = document.querySelectorAll('.btn');
const imgs = document.querySelectorAll('.image-to-show');
const span = document.querySelector('.span');
let counter = 0;
let setIntervalSwitchImg;

function switchImg (photos){
    photos[counter].classList.toggle('display-none');
    counter++;
    if (counter === photos.length){
        counter = 0;
        photos[counter].classList.toggle('display-none');
    }
    else{
        photos[counter].classList.toggle('display-none');
    }
}


function continueSwitchingImg (){
    setIntervalSwitchImg = setInterval(switchImg, 3000, imgs, counter);
}

continueSwitchingImg ();
console.log(setIntervalSwitchImg);
buttons[0].addEventListener("click", () => {
    clearInterval(setIntervalSwitchImg);
    });
buttons[1].addEventListener("click", () => continueSwitchingImg());